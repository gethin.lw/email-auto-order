#email-auto-order

This simple VBA script waits for a sharepoint share item object to appear in an
inbox, then queries a spreadsheet in a known location for a string matching
a defined pattern.

If the pattern is found, a corresponding cell is copied into
another spreadsheet and emailed as an attachment to a predefined email address.

This script is intended to run in Outlook, so it should understand Outlook 
objects, however for it to understand Excel objects (for spreadsheet access)
you need to have "Microsoft Excel 14.0 Object library" enabled 
in Tools->References.

