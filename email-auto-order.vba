Option Explicit
Private WithEvents inboxItems As Outlook.Items

Private Sub Application_Startup()
  Dim OutlookApp As Outlook.Application
  Dim objectNS As Outlook.NameSpace
  
  Set OutlookApp = Outlook.Application
  Set objectNS = OutlookApp.GetNamespace("MAPI")
  Set inboxItems = objectNS.GetDefaultFolder(olFolderInbox).Items
End Sub


Private Sub inboxItems_ItemAdd(ByVal Item As Object)
On Error GoTo ErrorHandler

Dim Msg As Outlook.MailItem
Dim MessageInfo
Dim Result

Dim wbk As Workbook


Dim i As Integer
Dim columnSize As Integer

Dim searchStringX1 As String
Dim numberFoundX1 As Integer
Dim productCodesX1() As String
Dim orderFormsX1() As String


Dim searchStringX270 As String
Dim numberFoundX270 As Integer
Dim productCodesX270() As String
Dim orderFormsX270() As String

Dim tempCellContents As String

Dim folderPath As String
Dim orderEmailAddress As String
Dim spreadSheetLocation As String


Dim stockSheetName As String
Dim orderSheetName As String


'---------Settings---------
'
'
' ***This will be the folder where your completed order forms are stored, 1 for each person
'*** They should be named like this: "Order form-John Doe.xlsx"
folderPath = "K:\email-order-script\"


searchStringX1 = "*think*pad*x1*carbon*"
searchStringX270 = "*think*pad*x270*"


' ******IMPORTANT: REMEMBER TO REDIM THE ARRAY WHEN NAMES ARE ADDED OR REMOVED!!!!
' (or it will cause an index error)
'
' This script might be running on an email account belonging to one of the people on this
' list, so the emailer subroutine will look out for one name (currently "John Doe") and change the
' content of the email slightly for it to make more sense when it's received by the sales company.
ReDim orderFormsX1(2)
orderFormsX1(0) = "John Doe"
orderFormsX1(1) = "Jane Doe"
orderFormsX1(2) = "Bob Doe"

' ******IMPORTANT: REMEMBER TO REDIM THE ARRAY WHEN NAMES ARE ADDED OR REMOVED!!!!
ReDim orderFormsX270(1)
orderFormsX270(0) = "Jane Doe"
orderFormsX270(1) = "Jim Doe"


' This is the email address that the order form should go to
orderEmailAddress = "sales@fake_order_company.co.uk "

' This is the location where the stock spreadsheet is stored (ie. the sharepoint file path)
spreadSheetLocation = "http://path.to.stock.spreadsheet/stock spreadsheet.xlsx"


stockSheetName = "Available Stock List"
orderSheetName = "Order Form"
'
'
'---------End of settings---------


' Only activate for SharingItem objects - this is what comes from SharePoint (they're not MailItems)
If TypeName(Item) = "SharingItem" Then

    If UCase(Item.Body) Like UCase("*Staff resale scheme.xlsx has been changed*") Then
    
        ' You need to have "Microsoft Excel 14.0 Object library" enabled in Tools->References for this to work
        Set wbk = Workbooks.Open(spreadSheetLocation)
        
        ' Look for the last row in column 1 that contains text
        columnSize = wbk.Sheets(stockSheetName).Cells(Rows.Count, 1).End(xlUp).Row
        
        numberFoundX1 = 0
        numberFoundX270 = 0
        
        ' Iterate through column D (starting at row 4) looking for a description that matches the equipment we're looking for
        ' If it's found, look in column A for the product code and add it to the productCodesX1 array
        For i = 4 To columnSize
        
            tempCellContents = wbk.Sheets(stockSheetName).Cells(i, 4).Value
        
            If UCase(tempCellContents) Like UCase(searchStringX1) Then

                numberFoundX1 = numberFoundX1 + 1
                ReDim Preserve productCodesX1(numberFoundX1 - 1)
                productCodesX1(numberFoundX1 - 1) = wbk.Sheets(stockSheetName).Cells(i, 1).Value
            
            End If
            
            If UCase(tempCellContents) Like UCase(searchStringX270) Then

                numberFoundX270 = numberFoundX270 + 1
                ReDim Preserve productCodesX270(numberFoundX270 - 1)
                productCodesX270(numberFoundX270 - 1) = wbk.Sheets(stockSheetName).Cells(i, 1).Value
            
            End If
            
        Next i
        
        ' Use the SubmitOrderForm function to send the emails for each product type
        If numberFoundX1 > 0 Then
            Call SubmitOrderForm(productCodesX1, orderFormsX1, "X1", folderPath, orderSheetName, orderEmailAddress)
        End If
        If numberFoundX270 > 0 Then
            Call SubmitOrderForm(productCodesX270, orderFormsX270, "X270", folderPath, orderSheetName, orderEmailAddress)
        End If
        
    End If

End If


ExitNewItem:
    Set wbk = Nothing

    Exit Sub

ErrorHandler:
    MsgBox "Error in Main: " & Err.Number & " - " & Err.Description
    Resume ExitNewItem

End Sub






Sub SubmitOrderForm(productCode As Variant, personBuying As Variant, thingBeingOrdered As String, folderPath As String, orderSheetName As String, orderEmailAddress As String)
On Error GoTo ErrorHandler

Dim OutlookApp As Object
Dim OutlookMail As Object

Dim wbk As Workbook

Dim tempFileName As String
Dim orderFormFileName As String
Dim tempBodyText As String
Dim tempSubjectText As String

Dim i As Integer


' Randomise the array to avoid ordering the first item (and reduce the chance or ordering
' the same as someone else who's using a script.
' Also do the same for personBuying to make it fairer.
Call ShuffleArrayInPlace(productCode)
Call ShuffleArrayInPlace(personBuying)

' Re-size the largest array to the same size as the smallest array (eg. if there are 5 products
' found but only 3 people ordering, ignore the last 2 products. Alternatively if there are 2
' products found but 4 people ordering, ignore the last 2 people since there's no product for them.)
If UBound(productCode) > UBound(personBuying) Then
    ReDim Preserve productCode(UBound(personBuying))
Else
    ReDim Preserve personBuying(UBound(productCode))
End If

' Open up an instance of outlook for mailing
Set OutlookApp = CreateObject("Outlook.Application")

For i = 0 To UBound(productCode)

    ' Reassemble the order form file name based on the name in personBuying
    orderFormFileName = "Order form-" & personBuying(i) & ".xlsx"

    ' Create a temp filename based on the order form file name and today's date
    tempFileName = folderPath & Format(Date, "yyyy-mm-dd") & " " & thingBeingOrdered & " " & orderFormFileName

    ' Only update the order form and carry out a mailing if a file with the temp filename
    ' *doesn't* exist. This is so that the order email address doesn't get multiple orders
    ' if the file is updated several times in one day (ie. each person is restricted to 1
    ' order per day)
    If Dir(tempFileName) = "" Then
        
        ' Open up the order form workbook, copy in the product number, and save it under the
        ' temp filename
        Set wbk = Workbooks.Open(folderPath & orderFormFileName)
        With wbk
            .Sheets(orderSheetName).Cells(16, 2).Value = productCode(i)
            .SaveAs (tempFileName)
            .Close
        End With

        ' Send an email to the order email address with the new workbook attached
        Set OutlookMail = OutlookApp.CreateItem(0)
        
        ' Set the body and subject text depending on whether the order is for the person hosting
        ' the script or someone else.
        If personBuying(i) = "John Doe" Then
        
            ' Don't bother with explanatory text
            tempBodyText = ""
            tempSubjectText = "IT equipment order - " & productCode(i)
        
        Else
        
            ' Put in some explanatory text to show that the order is being sent on behalf of someone else.
            tempBodyText = "Please find attached an order form being sent by me on behalf of " & _
                    personBuying(i) & " (contact details are in the form)." & vbCrLf & vbCrLf & "Thanks," & _
                    vbCrLf & vbCrLf & "John"
            tempSubjectText = "IT equipment order - " & productCode(i) & " sent on behalf of " & personBuying(i)
        
        End If
        
        ' Build the email and send
        With OutlookMail
            .To = orderEmailAddress
            .CC = ""
            .BCC = ""
            .Subject = tempSubjectText
            .Body = tempBodyText
            .Attachments.Add (tempFileName)
            .Send
        End With

    End If

Next i

    
ExitNewItem:
    Set wbk = Nothing
    Set OutlookApp = Nothing
    Set OutlookMail = Nothing

    Exit Sub

ErrorHandler:
    MsgBox "Error in SubmitOrderForm: " & Err.Number & " - " & Err.Description
    Resume ExitNewItem

End Sub





Sub ShuffleArrayInPlace(InArray As Variant)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ShuffleArrayInPlace
' This shuffles InArray to random order, randomized in place.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim N As Long
    Dim Temp As Variant
    Dim J As Long
   
    Randomize
    For N = LBound(InArray) To UBound(InArray)
        J = CLng(((UBound(InArray) - N) * Rnd) + N)
        If N <> J Then
            Temp = InArray(N)
            InArray(N) = InArray(J)
            InArray(J) = Temp
        End If
    Next N
End Sub
